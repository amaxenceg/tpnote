﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bibliothequecounterclass
{
    public class BasicCounter
    {
        private int compteur;
        public BasicCounter(int compteur)
        {
            this.compteur = compteur;
        }

        public void Incrementer()
        {
            this.compteur = compteur + 1;
        }

        public void Decrementer()
        {
            if (this.compteur != 0)
            {
                this.compteur = compteur - 1;
            }
        }

        public void RAZ()
        {
            this.compteur = 0;
        }
        public int getcompteur()
        {
            return (this.compteur);
        }
    }
}