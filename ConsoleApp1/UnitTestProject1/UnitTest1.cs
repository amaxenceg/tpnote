﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using bibliothequecounterclass;

namespace TestsCompteur
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestIncrementation()
        {
            BasicCounter valeur = new BasicCounter(0);
            valeur.Incrementer();
            Assert.AreEqual(1, valeur.getcompteur());
        }

        [TestMethod]
        public void TestDecrementation()
        {
            BasicCounter valeur = new BasicCounter(1);
            valeur.Decrementer();
            Assert.AreEqual(0, valeur.getcompteur());
        }

        [TestMethod]
        public void Testraz()
        {

            BasicCounter valeur = new BasicCounter(20);
            valeur.RAZ();
            Assert.AreEqual(0, valeur.getcompteur());
        }

    }
}